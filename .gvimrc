scriptencoding utf-8
" {{{
let g:vim_config_dir = get(g:, 'vim_config_dir', expand('~/.config/vim'))
let g:vim_config_gvimrc = g:vim_config_dir . '/gvimrc'
if filereadable(g:vim_config_gvimrc)
  execute 'source' g:vim_config_gvimrc
endif
" }}}
" {{{
" vim:set foldmethod=marker commentstring=//%s :
