" {{{ encoding
scriptencoding utf-8
" }}}
" {{{ vimrcリロード

if has('vim_starting')
  function s:reload_vimrc() abort
    if exists("dein#name")
      call dein#clear_state()
    endif
    execute printf('source %s', $MYVIMRC)
    if has('gui_running')
      execute printf('source %s', $MYGVIMRC)
    endif
    set foldmethod=marker
    redraw
    echo printf('.vimrc/.gvimrc has reloaded (%s).', strftime('%c'))
  endfunction
endif

" プラグインを含めてリロード
command! Reload call <SID>reload_vimrc()

let g:vimrc_autoreload = 0
" auto reload .vimrc
augroup source-vimrc
  autocmd!
  if get(g:, 'vimrc_autoreload')
    autocmd BufWritePost */.config/dein/*.toml Reload
    autocmd BufWritePost *vimrc,*/.config/*.vim Reload
endif
augroup END

" }}}
" {{{
" vim:set foldmethod=marker commentstring=//%s :
