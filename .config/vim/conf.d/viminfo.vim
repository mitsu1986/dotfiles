" {{{ encoding
scriptencoding utf-8
" }}}
" {{{ viminfoの変更
if (has("win32") || has ("win64") )
  let s:viminfo_path = substitute(expand(g:vim_cache_dir . '/viminfo_win'), "\\", "/", "g")
else
  let s:viminfo_path = expand(g:vim_cache_dir . '/viminfo')
endif
execute 'set viminfo+=n' . s:viminfo_path
" }}}
" {{{
" vim:set foldmethod=marker commentstring=//%s :
