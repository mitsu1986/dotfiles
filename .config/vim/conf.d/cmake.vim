" {{{ encoding
scriptencoding utf-8
" }}}
" {{{ LoadGuard

if exists('g:loaded_cmake') || &cp
  finish
endif
" let g:loaded_cmake = 1

let s:save_cpo = &cpo
set cpo&vim

"}}}
"{{{ Settings

let g:cmake_cache_dir = get(g:, 'cmake_cache_dir', expand('~/.cache/cmake/'))
let g:cmake_generator = get(g:, 'cmake_generator', '')
let g:cmake_build_type = get(g:, 'cmake_build_type', '')
"let b:cmake_generators = ''
"
nnoremap <silent> <C-B> :call cmake#Make()<CR>

"}}}
"{{{ Body
let s:scm_patterns = ['.git', '.git/', '_darcs/', '.hg/', '.bzr/', '.svn/', '.editorconfig']
let s:file_patterns = [ 'CMakeLists.txt' ]

" まずCMakeLists.txtを探す
function! s:FindScmDir()
  let l:current_file = expand('%:p')
  let l:current_dir = fnameescape(fnamemodify(l:current_file, ':h'))
  for pattern in s:scm_patterns
    if pattern =~ '\/\|\\'
      let l:match = finddir(l:pattern, l:current_dir . ';')
      if !empty(l:match)
        return fnamemodify(match, ':p:h:h')
      endif
    else
      let l:match = findfile(l:pattern, l:current_dir . ';')
      if !empty(l:match)
        return fnamemodify(match, ':p:h')
      endif
    endif
  endfor
  return ''
endfunction

function! s:FindCmakeListsDir()
  let l:scm_dir = s:FindScmDir()
  if empty(l:scm_dir)
    let l:current_dir = expand('%:p:h')
    for pattern in s:file_patterns
      let l:match = findfile(l:pattern, l:current_dir . ';')
      if !empty(l:match)
        return fnamemodify(match, ':p:h')
      endif
    endfor
  else
    for pattern in s:file_patterns
      let l:match = findfile(l:pattern, l:scm_dir)
      if !empty(l:match)
        return fnamemodify(match, ':p:h')
      endif
    endfor
  endif
  return ''
endfunction

let g:cmake_cache_dirs = {}
let s:cmake_info_file = expand(g:cmake_cache_dir . '/VimCmakeInfo.txt')

if filereadable(s:cmake_info_file)
  execute 'source' s:cmake_info_file
endif

augroup SaveVimCmakeInfo
  autocmd!
  autocmd VimLeavePre * call s:SaveCmakeInfo()
  function! s:SaveCmakeInfo()
    if !isdirectory(g:cmake_cache_dir)
      call mkdir(g:cmake_cache_dir, 'p')
    endif

    redir! => l:cmake_cache_dirs
      silent echo ":let g:cmake_cache_dirs = "
      silent echo g:cmake_cache_dirs
    redir END
    let l:cmake_cache_dirs = substitute(l:cmake_cache_dirs, '\%x00', '', "g")
    let l:options = [l:cmake_cache_dirs]
    call writefile(l:options, s:cmake_info_file)
  endfunction
augroup END

function! s:FindCmakeCacheDir()
  let b:cmake_lists_dir = get(b:, 'cmake_lists_dir', s:FindCmakeListsDir())
  if empty(b:cmake_lists_dir)
    return
  endif
  if has_key(g:cmake_cache_dirs, b:cmake_lists_dir)
    return g:cmake_cache_dirs[b:cmake_lists_dir]
  endif
  let l:dir = expand(g:cmake_cache_dir . '/' . sha256(reltime()[1]))
  while !empty(glob(l:dir))
    let l:dir = expand(g:cmake_cache_dir . '/' . sha256(reltime()[1]))
  endwhile
  let g:cmake_cache_dirs[b:cmake_lists_dir] = l:dir
  call mkdir(l:dir, 'p')
  call s:SaveCmakeInfo()
  return l:dir
endfunction

function! g:cmake#Make(...)
  try
    echomsg 'Make Starting...'
    if !executable('cmake')
      throw 'cmake is not found'
    endif
    let b:cmake_cache_dir = get(b:, 'cmake_cache_dir', s:FindCmakeCacheDir())
    let b:cmake_lists_dir = get(b:, 'cmake_lists_dir', s:FindCmakeListsDir())
    let l:cmake_generator = get(b:, 'cmake_generator', g:cmake_generator)
    let l:cmake_build_type = get(b:, 'cmake_build_type', g:cmake_build_type)
  
    if empty(b:cmake_lists_dir)
      throw 'CmakeLists.txt is not found'
    endif
  
    if empty(l:cmake_build_type)
      let l:cmake_build_type = 'Debug'
    endif
  
    if has('win32') || has('win64')
      if empty(l:cmake_generator)
        let l:cmake_generator = 'MinGW Makefiles'
      endif
    endif
  
    let l:make_executor = ''
    if l:cmake_generator == 'MinGW Makefiles'
      if executable('mingw32-make')
        let l:make_executor = 'mingw32-make'
      endif
    endif
    if empty(l:make_executor)
      if executable('make')
        let l:make_executor = 'make'
      endif
    endif
    if empty(l:make_executor)
      throw 'make is not found'
    endif
  
    let l:build_dir = expand(b:cmake_cache_dir . '/' . l:cmake_build_type)
    if !isdirectory(l:build_dir)
      call mkdir(l:build_dir, 'p')
    endif
  
    " Generate
    if !filereadable(expand(l:build_dir . '/Makefile'))
    echomsg 'CMake Execute...'
    "if !filereadable(expand(l:build_dir . '/CMakeCache.txt'))
      let l:opt = ""
      if !empty(l:cmake_generator)
        let l:opt = l:opt . ' -G "' . l:cmake_generator . '"'
      endif
      if !empty(l:cmake_build_type)
        let l:opt = l:opt . ' -DCMAKE_BUILD_TYPE=' . l:cmake_build_type
      endif
      let l:cmdline = 'cmake ' . l:opt . ' ' . b:cmake_lists_dir
      execute 'cd' l:build_dir
      let l:cmake_messages = system(l:cmdline)
  
      execute 'cd -'
      if v:shell_error != 0
        let l:errors = []
        for l in split(l:cmake_messages, "\n")
          let l:info = {}
          let l:info.text = l
          call add(errors, info)
        endfor
        call setqflist(errors, 'r')
        throw 'CMake Failed'
      endif
    endif
    " Build
    echomsg 'Make Execute...'
    let l:make_option = get(a:, 1, '')
    let l:make_messages = system(l:make_executor . ' -B -C "' . l:build_dir . '" ' . l:make_option)
    let l:errors = []
    let l:errors_default = []
    for l in split(l:make_messages, "\n")
      let l:info = {}
      if l =~ '^\(.\+\):\(\d\+\):\(\d\+\): \(.\+\): \(.\+\)'
        let l:mlist = matchlist(l, '^\(.\+\):\(\d\+\):\(\d\+\): \(.\+\): \(.\+\)')
        let l:info.filename = l:mlist[1]
        let l:info.lnum = l:mlist[2]
        let l:info.col = l:mlist[3]
        if l:mlist[4] == 'note'
          let l:info.type = 'i'
          let l:info.text = l:mlist[5]
        elseif l:mlist[4] == 'error'
          let l:info.type = 'e'
          let l:info.text = l:mlist[5]
        elseif l:mlist[4] == 'warning'
          let l:info.type = 'w'
          let l:info.text = l:mlist[5]
        else
          let l:info.text = l:mlist[4] . ': ' . l:mlist[5]
        endif
        call add(l:errors, l:info)
      else
        let l:info.text = l
        call add(l:errors_default, l:info)
      endif
    endfor
    if !empty(l:errors)
      call setqflist(errors, 'r')
    else
      call setqflist(errors_default, 'r')
    endif
    if v:shell_error != 0
      throw 'Make Failed'
    endif
    let l:curwindow = winnr()
    let l:curbuffer = winbufnr(l:curwindow)
    cwindow
    if l:curbuffer != winbufnr(l:curwindow)
      let l:curwindow = bufwinnr(l:curbuffer)
    endif
    if l:curwindow > 0
      execute l:curwindow . 'wincmd w'
    endif
    echomsg "Make Complete"
    return 1
  catch
    let l:curwindow = winnr()
    let l:curbuffer = winbufnr(l:curwindow)
    cwindow
    if l:curbuffer != winbufnr(l:curwindow)
      let l:curwindow = bufwinnr(l:curbuffer)
    endif
    if l:curwindow > 0
      execute l:curwindow . 'wincmd w'
    endif
    echohl ErrorMsg
    echomsg v:exception
    echohl None
    return 0
  endtry
endfunction

" }}}
" {{{
let &cpo = s:save_cpo
" vim:set foldmethod=marker commentstring=//%s :
