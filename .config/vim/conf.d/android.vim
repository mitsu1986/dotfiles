" {{{ encoding
scriptencoding utf-8
" }}}
" {{{
if exists("android") && exists("*ATEMod")

set mouse=a

" ノーマルモード移行で英数確定入力(ソフトキーボード)

" エスケープシーケンス
" |    | 効果           | 想定使用状況                | 備考
" | 50 | デフォルト     |                             |
" | 51 | 英数確定入力   | IMEが英数確定入力でない場合 | *日本語入力不可
" | 52 | 英数入力に設定 | IMEが英数確定入力の場合     |
" | 53 | 英数確定入力   | Google日本語入力の場合      | *Vimノーマルモード専用
" ※Google日本語入力の英数確定入力は実験的なものです
"
" 実際の効果は
" echo -n -e "\0033[51t"
" のようにshellから実行して確認してみてください。
" Vimからは :ATEMod 51 で実行可能です。
" ただし「設定」のインプットメソッドが「単語ベース」でないと効果はありません。
"
" | 5  | 自動設定(ノーマルモード) | *ATOK=52, その他=51
" | 55 | 自動設定(挿入モード)     | *Wnn系=52, その他=50

" 以下のコメントのaugroupまでを有効化すると自動英数確定入力になります
" ATOKとWnn Keyboard Labでは自動設定で特に問題ないようですが、環境に応じてスク
" リプト設定を適切に変更してみてください。
"
" ノーマルモード移行で英数入力(ソフトキーボード)
" ノーマルモード
let s:ImeNormal = 5
" " 挿入モード
let s:ImeInsert = 55
" " Vim終了後
let s:ImeVimLeave = 50
"
augroup AndroidIME
  au!
  au VimEnter    * call ATEMod(s:ImeNormal)
  au InsertEnter * call ATEMod(s:ImeInsert)
  au InsertLeave * call ATEMod(s:ImeNormal)
  au CmdWinEnter * call ATEMod(s:ImeInsert)
  au CmdWinLeave * call ATEMod(s:ImeNormal)
  au VimLeavePre * call ATEMod(s:ImeVimLeave)
augroup END

endif
" }}}
" {{{
" vim:set foldmethod=marker commentstring=//%s :
