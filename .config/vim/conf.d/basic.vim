" {{{ encoding
scriptencoding utf-8
" }}}
" {{{ 基本設定
"
" ####################
" ##### 表示設定 #####
" ####################

" ## ターミナルのタイトルをセットする
set title

" ## 行番号を表示する
set number

" ## カーソルの回り込みができるようになる
set whichwrap=b,s,[,],<,> 

" ## バックスペースを、空白、行末、行頭でも使えるようにする
set backspace=indent,eol,start

set wildmenu

" カラー設定
set t_Co=256

set background=light

" colorscheme Tomorrow

if has('syntax')
    set spelllang=en,cjk
endif

" if has('filetype')
filetype plugin indent on
" endif

" ##########################
" ##### 空白・タブ設定 #####
" ##########################

" ## 文脈によって解釈が異なる全角文字の幅を、2に固定する
set ambiwidth=double

" ## タブ幅をスペース4つ分にする
set tabstop=4

" ## tabを半角スペースで挿入する
set expandtab

" ## vimが自動で生成する（読み込み時など）tab幅をスペース4つ文にする
set shiftwidth=4

" ## 空白文字の可視化
set list
set listchars=tab:▸\ ,eol:↲,extends:»,precedes:«,nbsp:%
" set listchars=tab:»-,eol:↲,extends:»,precedes:«,nbsp:%
" set listchars=tab:»-,trail:-,eol:↲,extends:»,precedes:«,nbsp:%
" set listchars=tab:▸\ ,eol:¬

" ##########################
" ##### インデント設定 #####
" ##########################

" ## 改行時などに、自動でインデントを設定してくれる
set smartindent

" ## 新しい行を作った時に高度な自動インデントを行う
set smarttab

" ####################
" ##### 検索設定 #####
" ####################

" ## インクリメンタルサーチを行う
set incsearch

" ## 大文字/小文字の区別なく検索する
set ignorecase

" ## 検索文字列に大文字が含まれている場合は区別して検索する
set smartcase

" ## 検索時に最後まで行ったら最初に戻る
set wrapscan

" ## grep検索を設定する
set grepformat=%f:%l:%m,%f:%l%m,%f\ \ %l%m,%f 
set grepprg=grep\ -nh

" 検索結果をハイライト表示
set hlsearch

" ##########################
" ##### その他基本設定 #####
" ##########################

" ## "0"で始まる数値を、8進数として扱わないようにする
set nrformats-=octal

" ## ファイルの保存をしていなくても、べつのファイルを開けるようにする
set hidden

set history=50

" ## クリップボードをOSと連携する
if has('gui') || has('xterm_clipboard')
    set clipboard=unnamed
endif

" ## 閉括弧が入力された時、対応する括弧を強調する
set showmatch
set matchtime=1

" ## 入力中のコマンドをステータスに表示する
set showcmd

" ## カーソル行を強調表示
"set cursorline

" ## python3パス
let g:python3_host_prog = '/usr/local/bin/python3'

" ## スクロールする時に下が見えるようにする
set scrolloff=5

" ## ビープ音を消す
set vb t_vb=
set novisualbell
set noerrorbells

" VI互換 OFF
if &compatible
  set nocompatible
endif

" IME設定
set iminsert=0
set imsearch=0
" set imdisable

if has('multi_byte_ime') || has('xim') || has('gui_macvim')
  " 挿入モードでのIME状態を記憶させない
  inoremap <silent> <ESC> <ESC>:set iminsert=0<CR>
  
  if exists('+imdisableactivate')
    let IM_CtrlMacVimKaoriya = 0
    set noimdisableactivate
    " set imdisableactivate
  endif
  
  augroup EnableIME
    autocmd!
    autocmd InsertEnter * call s:EnableIme()
    " autocmd InsertLeave * call s:DisableIme()
    autocmd CmdwinEnter * call s:EnableIme()
    " autocmd CmdwinLeave * call s:DisableIme()
    " autocmd WinEnter * call s:DisableIme()
    autocmd InsertLeave * call s:ImeOff()
    autocmd CmdwinLeave * call s:ImeOff()
    autocmd WinEnter * call s:ImeOff()
    autocmd ColorScheme * call s:SetImeColor()
  augroup END
endif

function! s:ImeOff()
  set imdisable
  set noimdisable
endfunction

function! s:EnableIme()
  set noimdisable
endfunction

function! s:DisableIme()
  set imdisable
endfunction

function! s:SetImeColor()
  "## IME状態に応じたカーソル色を設定
  if has('multi_byte_ime') || has('xim')
    " highlight Cursor guifg=NONE guibg=Green
    " highlight CursorIM guifg=NONE guibg=Green
    highlight def link CursorIM Search
  endif
endfunction

" ウィンドウの幅より長い行は折り返され、次の行に続けて表示される
set wrap

" イントロ画面非表示
set shortmess+=I

" バッファが変更されているとき、コマンドをエラーにするのでなく、保存する
" かどうか確認を求める
set confirm

" マウス有効化
set mouse=a

set autoread

" ##############################
" ########## 特殊設定 ##########
" ##############################

" ## swapファイルは常にreadonlyで開く
augroup swapchoice-readonly
    autocmd!
    autocmd SwapExists * let v:swapchoice = 'o'
augroup END

" 全角スペースの表示
function! ZenkakuSpace()
    " highlight ZenkakuSpace cterm=reverse ctermfg=DarkGray gui=reverse guifg=DarkGray
    highlight def link ZenkakuSpace Visual
endfunction

if has('syntax')
    augroup ZenkakuSpace
        autocmd!
        "ZenkakuSpace をカラーファイルで設定するなら、
        "次の行をコメントアウト
        autocmd ColorScheme       * call ZenkakuSpace()
        autocmd VimEnter,WinEnter * match ZenkakuSpace /　/
        autocmd VimEnter,WinEnter * match ZenkakuSpace '\%u3000'
    augroup END
    call ZenkakuSpace()
endif

" WinではPATHに$VIMが含まれていないときにexeを見つけ出せないので修正
if has('win32') && $PATH !~? '\(^\|;\)' . escape($VIM, '\\') . '\(;\|$\)'
  let $PATH = $VIM . ';' . $PATH
endif

if has('mac')
  " Macではデフォルトの'iskeyword'がcp932に対応しきれていないので修正
  set iskeyword=@,48-57,_,128-167,224-235
endif

" grep → QuickFix
augroup QuickFixCmd
  autocmd!
  autocmd QuickFixCmdPost *grep* cwindow
augroup END

" よくわからないが、一度IMEをDisableにし、EnableにするとCursorIMが効くように
" なった。
if has('mac') && has('xim')
  augroup MacIM
    autocmd!
    autocmd VimEnter * call s:ImeOff()
  augroup END
endif

"拡張属性を自動付与
function! s:autoadd_extended_attribute()
  if has('mac')
    if &fenc=='utf-8'
      exec "silent !xattr -w com.apple.TextEncoding 'UTF-8;134217984' \"%\"" |
    elseif &fenc=='euc-jp' |
      exec "silent !xattr -w com.apple.TextEncoding 'EUC-JP;2361' \"%\"" |
    elseif &fenc=='iso-2022-jp' |
      exec "silent !xattr -w com.apple.TextEncoding 'ISO-2022-JP;2080' \"%\"" |
    elseif &fenc=='cp932' |
      exec "silent !xattr -w com.apple.TextEncoding 'SHIFT_JIS;2561' \"%\"" |
    endif
  endif
endfunction

if has('mac')
  augroup AddExtendAttribute
    autocmd!
    autocmd BufWritePost * call <SID>autoadd_extended_attribute()
  augroup END
endif

if has('win32')
  "パスのセパレータを変更(\->/)
  set shellslash
  "スペースの入ったファイル名も扱えるようにする
  set isfname+=32
endif

if has('win32') && has('gui')
  "ファイル保存ダイアログの初期ディレクトリをバッファのあるディレクトリにする
  set browsedir=buffer
endif

function! s:QuickFix_Exit_OnlyWindow()
  if winnr('$') == 1
    if (getbufvar(winbufnr(0), '&buftype')) == 'quickfix'
      quit
    endif
  endif
endfunction
augroup QFExitOW
  autocmd!
  autocmd WinEnter * call s:QuickFix_Exit_OnlyWindow()
augroup END

function! s:WinBuffersUpdate()
  let w:buffers = get(w:, 'buffers', [])
  let i = bufnr('%')
  if index(w:buffers, i) == -1
    call add(w:buffers, i)
  endif
  unlet i
endfunction

augroup WinBuffersUpdate
  autocmd!
  autocmd BufWinEnter * call s:WinBuffersUpdate()
augroup END

function! s:WinBuffersNext()
  let l:nbi = index(w:buffers, bufnr('%'))
  if l:nbi == len(w:buffers) - 1
    let l:nbi = 0
  else
    let l:nbi = l:nbi + 1
  endif
  let l:nb = get(w:buffers, l:nbi)
  " while !buflisted(l:nb)
  while !bufloaded(l:nb)
    if l:nbi == len(w:buffers) - 1
      let l:nbi = 0
    else
      let l:nbi = l:nbi + 1
    endif
    let l:nb = get(w:buffers, l:nbi)
  endwhile

  execute 'buffer' l:nb
endfunction

function! s:WinBuffersPrev()
  let l:nbi = index(w:buffers, bufnr('%'))
  if l:nbi == 0
    let l:nbi = len(w:buffers) - 1
  else
    let l:nbi = l:nbi - 1
  endif
  let l:nb = get(w:buffers, l:nbi)
  " while !buflisted(l:nb)
  while !bufloaded(l:nb)
    if l:nbi == 0
      let l:nbi = len(w:buffers) - 1
    else
      let l:nbi = l:nbi - 1
    endif
    let l:nb = get(w:buffers, l:nbi)
  endwhile

  execute 'buffer' l:nb
endfunction

function! s:WinBuffersDelete()
  let l:cbn = bufnr('%')
  call s:WinBuffersNext()
  call filter(w:buffers, 'v:val != ' . l:cbn)
  execute 'bdelete' l:cbn
endfunction

command! Bnext call <SID>WinBuffersNext()
command! Bprev call <SID>WinBuffersPrev()
command! Bdelete call <SID>WinBuffersDelete()

" retrun quickfix window visibled
function! g:Tabqf_CWindowVisible()
  for i in range(1, winnr('$'))
    if (getbufvar(winbufnr(i), '&buftype')) == 'quickfix'
      return 1
    endif
  endfor
  return 0
endfunction

" #################################
" ########### nvimの設定 ##########
" #################################
if has('nvim')
  let $NVIM_TUI_ENABLE_TRUE_COLOR=1
endif


" }}}
" {{{
" vim:set foldmethod=marker commentstring=//%s :
