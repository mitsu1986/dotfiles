" {{{ encoding
scriptencoding utf-8
" }}}
" {{{
" ########################################
" ########## キーマッピング設定 ##########
" ########################################

" ## マッピング Y を行末までのヤンクにする
nnoremap Y y$

" ## increment decrement
nnoremap + <C-a>
nnoremap - <C-x>

" ## PageUp, PageDown に設定
" nnoremap <PageDown> <C-F>
" nnoremap <PageUp> <C-B>
nnoremap <PageDown> <C-D>
nnoremap <PageUp> <C-U>
" nnoremap <PageDown> <C-E><C-E><C-E><C-E><C-E><C-E><C-E><C-E><C-E><C-E><C-E><C-E><C-E><C-E><C-E><C-E>
" nnoremap <PageUp> <C-Y><C-Y><C-Y><C-Y><C-Y><C-Y><C-Y><C-Y><C-Y><C-Y><C-Y><C-Y><C-Y><C-Y><C-Y><C-Y>

" バッファ切り替え
set <C-Right>=^[[C
set <C-Left>=^[[D
" set <C-S-Right>=^[[1;6C
" set <C-S-Left>=^[[1;6D

nnoremap <silent> <C-Left> :Bprev<CR>
nnoremap <silent> <C-Right> :Bnext<CR>
inoremap <silent> <C-Right> <ESC>:Bnext<CR>
inoremap <silent> <C-Left> <ESC>:Bprev<CR>
cnoremap <silent> <C-Right> <C-u>Bnext<CR>
cnoremap <silent> <C-Left> <C-u>Bprev<CR>

" タブ移動
nnoremap <silent> <C-Tab> :tabnext<CR>
nnoremap <slient> <C-S-Tab> :tabprevious<CR>
inoremap <silent> <C-Tab> <ESC>:tabnext<CR>
inoremap <slient> <C-S-Tab> <ESC>:tabprevious<CR>
cnoremap <silent> <C-Tab> <C-u>:tabnext<CR>
cnoremap <slient> <C-S-Tab> <C-u>:tabprevious<CR>

" ウインドウ移動
" nnoremap <silent> <C-S-Right> <C-w>w
nnoremap <silent> <M-Left> <C-w>h
nnoremap <silent> <M-Up> <C-w>k
nnoremap <silent> <M-Right> <C-w>l
nnoremap <silent> <M-Down> <C-w>j
nnoremap <silent> <Tab> <C-w>w

" バッファを閉じる
nnoremap <silent> <C-w> :Bd<CR>

" リロード
nnoremap <silent> <Leader>r :Reload<CR>

" 日本語入力時に悪さをする？為無効化
" カッコ補完
" inoremap { {}<Left>
" inoremap [ []<Left>
" inoremap ( ()<Left>
function! QuotesCompletion(char)
    let counter = 0
    let line = getline(".")
    for i in range(strlen(line))
        if line[i] == a:char
            let counter += 1
        endif
    endfor
    if counter % 2 == 0
        return a:char.a:char."\<Left>"
    else
        return a:char
    end
endfunction

" inoremap <silent> <expr> " QuotesCompletion("\"")
" inoremap <silent> <expr> ' QuotesCompletion("\'")

" }}}
" {{{
" vim:set foldmethod=marker commentstring=//%s :
