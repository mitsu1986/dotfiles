" {{{ encoding
scriptencoding utf-8
" }}}
" {{{ Folder Setting

" ## バックアップファイル保存先
let s:vim_backup_dir = expand(g:vim_cache_dir. '/vimbackup')
execute 'set backupdir=' . s:vim_backup_dir
if !isdirectory(s:vim_backup_dir)
  call mkdir(s:vim_backup_dir, "p")
endif

" ## Undoファイル保存先
let s:vim_undo_dir = expand(g:vim_cache_dir. '/vimundo')
execute 'set undodir=' . s:vim_undo_dir
if !isdirectory(s:vim_undo_dir)
  call mkdir(s:vim_undo_dir, "p")
endif

" ## スワップファイル保存先
let s:vim_undo_dir = s:vim_backup_dir
execute 'set directory=' . s:vim_undo_dir
if !isdirectory(s:vim_undo_dir)
  call mkdir(s:vim_undo_dir, "p")
endif

" }}}
" {{{
" vim:set foldmethod=marker commentstring=//%s :
