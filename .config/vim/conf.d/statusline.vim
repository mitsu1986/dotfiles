" {{{ encoding
scriptencoding utf-8
" }}}
" {{{
" ##########################################
" ########## ステータスライン設定 ##########
" ##########################################

" ## ステータスラインを常に表示
set laststatus=2


" ## 以下の設定はLightLineを使うので削除

" ## ルーラーの表示
" set ruler

" set statusline=%{g:buftabs_list}%=\ %m%r%h%w%y%{'['.(&fenc!=''?&fenc:&enc).']['.&ff.']'}\ %l,%c\ %P
" set statusline=%=\ [%{(&fenc!=''?&fenc:&enc)}/%{&ff}]\[%Y]\[%04l,%04v][%p%%]
" set statusline=%=\ %m%r%h%w%y%{'['.(&fenc!=''?&fenc:&enc).']['.&ff.']'}\ %l,%c\ %P
" set statusline=%=\ [%{(&fenc!=''?&fenc:&enc)}]\[%{&ff}]\ %l,%c\ %p%%

" }}}
" {{{
" vim:set foldmethod=marker commentstring=//%s :
