" {{{ encoding
scriptencoding utf-8
" }}}
" {{{ Syntax Setting
if has('syntax') || !syntax_on
  syntax on
endif
" }}}
" {{{
" vim:set foldmethod=marker commentstring=//%s :
