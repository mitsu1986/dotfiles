" {{{ encoding
scriptencoding utf-8
" }}}
" {{{ dein.vim設定

" プラグインが実際にインストールされるディレクトリ
let s:dein_config_dir = expand(g:vim_config_dir . '/dein')
let s:dein_dir = expand(g:vim_cache_dir . '/dein')

" dein.vim 本体
let s:dein_repo_dir = expand(s:dein_dir . '/repos/github.com/Shougo/dein.vim')

" dein.vim がなければ github から落としてくる
if &runtimepath !~# '/dein.vim'
  if !isdirectory(s:dein_repo_dir)
    execute '!git clone https://github.com/Shougo/dein.vim' s:dein_repo_dir
  endif
  execute 'set runtimepath+=' . s:dein_repo_dir
endif

" " Vim起動完了時にインストール
" augroup PluginInstall
"   autocmd!
"   autocmd VimEnter * if dein#check_install() | call dein#install() | endif
" augroup END

" プラグインリストを収めた TOML ファイル
let s:toml      = s:dein_config_dir . '/dein.toml'
let s:lazy_toml = s:dein_config_dir . '/dein_lazy.toml'

" 設定開始
if dein#load_state(s:dein_dir)
  call dein#begin(s:dein_dir, [$MYVIMRC, s:toml, s:lazy_toml])

  " TOML を読み込み、キャッシュしておく
  if filereadable(s:toml)
    call dein#load_toml(s:toml,      {'lazy': 0})
  endif
  if filereadable(s:lazy_toml)
    call dein#load_toml(s:lazy_toml, {'lazy': 1})
  endif

  " 設定終了
  call dein#end()
  call dein#save_state()
endif

if dein#check_install()
    call dein#install()
endif

" }}}
" {{{
" vim:set foldmethod=marker commentstring=//%s :
