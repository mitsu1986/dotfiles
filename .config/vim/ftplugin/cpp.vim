" インクルードパスを設定する
" gf などでヘッダーファイルを開きたい場合に影響する
setlocal path+=/usr/local/include

" 括弧を構成する設定に <> を追加する
" template<> を多用するのであれば
setlocal matchpairs+=<:>

" 最後に定義された include 箇所へ移動してを挿入モードへ
" nnoremap <buffer><silent> <Space>ii :execute "?".&include<CR> :noh<CR> o

" BOOST_PP_XXX 等のハイライトを行う
syntax match boost_pp /BOOST_PP_[A-z0-9_]*/
highlight link boost_pp cppStatement

" コメント形式
set commentstring=//\ %s


