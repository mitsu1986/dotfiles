scriptencoding utf-8
" {{{
let g:vim_config_dir = get(g:, 'vim_config_dir', expand('~/.config/vim'))
let g:vim_config_vimrc = g:vim_config_dir . '/vimrc'
if filereadable(g:vim_config_vimrc)
  let $VIM = g:vim_config_dir
  execute 'source' g:vim_config_vimrc
endif
" }}}
" {{{
" vim:set foldmethod=marker commentstring=//%s :
